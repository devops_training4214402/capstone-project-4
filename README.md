**Capstone Project 4: Deploy MS application in K8s**

- The link for the project used in this lecture can be found here: [https://github.com/techworld-with-nana/microservices-demo](https://github.com/techworld-with-nana/microservices-demo)﻿
- The config file used for the microservices demo can be found here: [https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/helm-chart-microservices/-/blob/main/config.yaml](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/helm-chart-microservices/-/blob/main/config.yaml?ref_type=heads)

# Ricardo Note's

Git URL
git@gitlab.com:devops_training4214402/capstone-project-4.git
https://gitlab.com/devops_training4214402/capstone-project-4.git

The template included Load Balancer type

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  type: LoadBalancer
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
```

New values for front end service for testing
```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  type: NodePort
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
    nodePort: 30007
```

##### Installation steps to install kubectl in a new VM (debian machine)

1. Update the `apt` package index and install packages needed to use the Kubernetes `apt` repository:
    
    ```shell
    sudo apt-get update
    # apt-transport-https may be a dummy package; if so, you can skip that package
    sudo apt-get install -y apt-transport-https ca-certificates curl
    ```
    
2. Download the public signing key for the Kubernetes package repositories. The same signing key is used for all repositories so you can disregard the version in the URL:
    
    ```shell
    # If the folder `/etc/apt/keyrings` does not exist, it should be created before the curl command, read the note below.
    # sudo mkdir -p -m 755 /etc/apt/keyrings
    curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    ```
    

**Note:** In releases older than Debian 12 and Ubuntu 22.04, folder `/etc/apt/keyrings` does not exist by default, and it should be created before the curl command.

3. Add the appropriate Kubernetes `apt` repository. If you want to use Kubernetes version different than v1.29, replace v1.29 with the desired minor version in the command below:
    
    ```shell
    # This overwrites any existing configuration in /etc/apt/sources.list.d/kubernetes.list
    echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
    ```
    

**Note:** To upgrade kubectl to another minor release, you'll need to bump the version in `/etc/apt/sources.list.d/kubernetes.list` before running `apt-get update` and `apt-get upgrade`. This procedure is described in more detail in [Changing The Kubernetes Package Repository](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/change-package-repository/).

4. Update `apt` package index, then install kubectl:
    
    ```shell
    sudo apt-get update
    sudo apt-get install -y kubectl
    ```


## Steps follow to deploy the K8s cluster

```
radisys@radisys-Standard-PC-i440FX-PIIX-1996:~/devops_training/capstone-project-4$ export KUBECONFIG=/home/radisys/devops_training/capstone-project-4/online-shop-microservices-kubeconfig.yaml
radisys@radisys-Standard-PC-i440FX-PIIX-1996:~/devops_training/capstone-project-4$ kubectl get node
NAME                            STATUS   ROLES    AGE   VERSION
lke152799-223677-16bb648c0000   Ready    <none>   10m   v1.28.3
lke152799-223677-2a90962d0000   Ready    <none>   10m   v1.28.3
lke152799-223677-5f06867a0000   Ready    <none>   10m   v1.28.3
radisys@radisys-Standard-PC-i440FX-PIIX-1996:~/devops_training/capstone-project-4$ kubectl create ns microservices
namespace/microservices created
radisys@radisys-Standard-PC-i440FX-PIIX-1996:~/devops_training/capstone-project-4$ kubectl apply -f config.yaml -n microservices
deployment.apps/emailservice created
service/emailservice created
deployment.apps/recommendationservice created
service/recommendationservice created
deployment.apps/productcatalogservice created
service/productcatalogservice created
deployment.apps/paymentservice created
service/paymentservice created
deployment.apps/currencyservice created
service/currencyservice created
deployment.apps/shippingservice created
service/shippingservice created
deployment.apps/adservice created
service/adservice created
deployment.apps/cartservice created
service/cartservice created
deployment.apps/redis-cart created
service/redis-cart created
deployment.apps/checkoutservice created
service/checkoutservice created
deployment.apps/frontend created
The Service "frontend" is invalid: spec.type: Unsupported value: "Nodeport": supported values: "ClusterIP", "ExternalName", "LoadBalancer", "NodePort"
radisys@radisys-Standard-PC-i440FX-PIIX-1996:~/devops_training/capstone-project-4$ kubectl apply -f config.yaml -n microservices
deployment.apps/emailservice unchanged
service/emailservice unchanged
deployment.apps/recommendationservice unchanged
service/recommendationservice unchanged
deployment.apps/productcatalogservice unchanged
service/productcatalogservice unchanged
deployment.apps/paymentservice unchanged
service/paymentservice unchanged
deployment.apps/currencyservice unchanged
service/currencyservice unchanged
deployment.apps/shippingservice unchanged
service/shippingservice unchanged
deployment.apps/adservice unchanged
service/adservice unchanged
deployment.apps/cartservice unchanged
service/cartservice unchanged
deployment.apps/redis-cart unchanged
service/redis-cart unchanged
deployment.apps/checkoutservice unchanged
service/checkoutservice unchanged
deployment.apps/frontend unchanged
service/frontend created
```

Error found in emailservice microservice
```bash
NAME                                     READY   STATUS    RESTARTS       AGE
adservice-69bf6b6cc4-gqjqq               1/1     Running   0              6m41s
adservice-69bf6b6cc4-vxvmq               1/1     Running   0              6m41s
cartservice-549bcd99c7-gpcjv             1/1     Running   0              6m40s
cartservice-549bcd99c7-wzb9w             1/1     Running   0              6m40s
checkoutservice-5cf88777bd-2czs5         1/1     Running   0              6m38s
checkoutservice-5cf88777bd-2dfcr         1/1     Running   0              6m38s
currencyservice-7876c4645b-l5nhb         1/1     Running   0              6m43s
currencyservice-7876c4645b-pn5bg         1/1     Running   0              6m43s
emailservice-7bc474749-n5gg9             1/1     Running   6 (2m7s ago)   6m44s
frontend-58c57d98cf-7tz9p                1/1     Running   0              6m38s
frontend-58c57d98cf-zbq9t                1/1     Running   0              6m38s
paymentservice-7476b8d844-hpwjt          1/1     Running   0              6m43s
paymentservice-7476b8d844-s9b6n          1/1     Running   0              6m43s
productcatalogservice-69c85d9574-4z48h   1/1     Running   0              6m43s
productcatalogservice-69c85d9574-9b96d   1/1     Running   0              6m43s
recommendationservice-667cc86cc4-5lzf5   1/1     Running   0              6m43s
recommendationservice-667cc86cc4-dsf6t   1/1     Running   0              6m43s
redis-cart-7896b967d6-d6bvp              1/1     Running   0              6m39s
redis-cart-7896b967d6-rlhks              1/1     Running   0              6m39s

```

Reason kubectl describe
```bash
Events:
  Type     Reason     Age    From               Message
  ----     ------     ----   ----               -------
  Normal   Scheduled  5m53s  default-scheduler  Successfully assigned microservices/recommendationservice-667cc86cc4-dsf6t to lke152799-223677-16bb648c0000
  Normal   Pulling    5m52s  kubelet            Pulling image "gcr.io/google-samples/microservices-demo/recommendationservice:v0.8.0"
  Normal   Pulled     5m47s  kubelet            Successfully pulled image "gcr.io/google-samples/microservices-demo/recommendationservice:v0.8.0" in 5.128s (5.128s including waiting)
  Normal   Created    5m47s  kubelet            Created container service
  Normal   Started    5m47s  kubelet            Started container service
  Warning  Unhealthy  5m45s  kubelet            Readiness probe failed: timeout: failed to connect service "10.2.0.131:8080" within 1s: context deadline exceeded

```
Frontend running in 
http://45.33.10.103:30007/

![[Pasted image 20240124175824.png]]

Correcting the yaml to Loadbalancer to comply with the security best practices 
```
---
apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  type: LoadBalancer
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
```

```
radisys@radisys-Standard-PC-i440FX-PIIX-1996:~/devops_training/capstone-project-4$ kubectl apply -f config.yaml -n microservices
deployment.apps/emailservice unchanged
service/emailservice unchanged
deployment.apps/recommendationservice unchanged
service/recommendationservice unchanged
deployment.apps/productcatalogservice unchanged
service/productcatalogservice unchanged
deployment.apps/paymentservice unchanged
service/paymentservice unchanged
deployment.apps/currencyservice unchanged
service/currencyservice unchanged
deployment.apps/shippingservice unchanged
service/shippingservice unchanged
deployment.apps/adservice unchanged
service/adservice unchanged
deployment.apps/cartservice unchanged
service/cartservice unchanged
deployment.apps/redis-cart unchanged
service/redis-cart unchanged
deployment.apps/checkoutservice unchanged
service/checkoutservice unchanged
deployment.apps/frontend unchanged
service/frontend configured
radisys@radisys-Standard-PC-i440FX-PIIX-1996:~/devops_training/capstone-project-4$
```

![[Pasted image 20240124183331.png]]
